## Install dependencies
```
npm install
```

## Run server locally on port 3000
```
npm run dev
```

## Deploy
### Setup
Configuration file: `ecosystem.config.js`

```
pm2 deploy dev setup
```

### Deploy
```
pm2 deploy dev
```