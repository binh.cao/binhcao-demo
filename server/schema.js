const {makeExecutableSchema} = require('graphql-tools')
import UserSchema from './features/user/schema'
import BillSchema from './features/bill/schema'
import UserResolver from './features/user/resolvers'
import BillResolver from './features/bill/resolvers'

let typeDefs = `
  ${BillSchema.types}
  ${UserSchema.types}
  
  type Mutation {
    ${BillSchema.mutations}
    ${UserSchema.mutations}
  }
  
  type Query {
    ${BillSchema.queries}
    ${UserSchema.queries}
  }
`

let queries = Object.assign({}, UserResolver.queries, BillResolver.queries)
let mutations = Object.assign({}, UserResolver.mutations, BillResolver.mutations)
let data = Object.assign({}, UserResolver.data, BillResolver.data)

let resolvers = {
  Query: queries,
  Mutation: mutations
}

resolvers = Object.assign(resolvers, data)

module.exports = makeExecutableSchema({typeDefs, resolvers})