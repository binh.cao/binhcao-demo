import stricts from './cross-env/stricts'
import amazons from './cross-env/amazons'
import files from './cross-env/files'
import qrcodes from './cross-env/qrcodes'
import limitations from './cross-env/limitations'

let common = {
  env: {
    production: 'production',
    development: 'development',
    test: 'test'
  },
  locales: {
    all: 'en vi',
    en: 'en',
    vi: 'vi'
  },
  city: {
    list: 'all da-nang ho-chi-minh',
    all: 'da-nang ho-chi-minh',
    daNang: 'da-nang',
    hoChiMinh: 'ho-chi-minh'
  },
  conventions: {
    number: 0,
    array: [],
    string: '',
    object: null
  },

  gender: {
    list: 'all male female',
    all: 'all',
    male: 'male',
    female: 'female'
  },

  user: {
    roles: {
      authenticated: 'authenticated',
      admin: 'admin',
      business: 'business',
      customercare: 'customercare',
      sale: 'sale',
      staff: 'staff',
      chainManager: 'chainManager'
    },
    forgotPasswordTokenExpireTime: 86400000, // 1 day
    maxVerifySMS: 5, // per day
    integratedId: {
      tch: 'tch', // The Coffee House
      zodyCard: 'zodyCard'
    },
    suspicion: {
      reasons: {
        all: 'bigBill multipleBillInDay continuousBillIn5Days bigSpendInMonth',
        bigBill: 'bigBill',
        multipleBillInDay: 'multipleBillInDay',
        continuousBillInMultipleDays: 'continuousBillInMultipleDays',
        bigSpendInMonth: 'bigSpendInMonth'
      },
      queryType: {
        all: 'registered unregistered',
        registered: 'registered',
        unregistered: 'unregistered'
      }
    }
  },
  bill: {
    // removeExpireTime: 43200000, // 12 hours
    removeExpireTime: 0, // TODO fix to anti cheat
    source: {
      zody: 'zody',
      ipos: 'ipos',
      rkeeper: 'rkeeper',
      foodyPOS: 'foodyPOS',
      tch: 'tch',
      shine30: 'shine30',
      quanso: 'quanso'
    }
  },
  referral: {
    success: 100,
    bonusForInviterWhenInviteeInputSuccess: 0,
    input: 50,
    max: 200,
    modifyTimes: 1,
    host: 'https://zody.vn/dl/share'
  },
}

Object.assign(common, stricts, amazons, files, qrcodes, limitations)

export default common