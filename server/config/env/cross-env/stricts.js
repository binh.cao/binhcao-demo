export default {
  // Strict data
  strict: {
    hexaColorPattern: /^#[0-9a-f]{6}$/i,
    objectId: /^[0-9a-fA-F]{24}$/,
    price: /^[1-9][0-9]*$/,
    phone: /^\+?1?(\d{10,12}$)/,
    url: /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i,
    business: {
      nameMinLength: 3,
      nameMaxLength: 128
    },
    user: {
      nameMinLength: 2,
      nameMaxLength: 64,
      pwdMinLength: 6,
      defaultAge: 20
    },
    referral: {
      minLength: 6,
      maxLength: 10
    },
    zcoin: {
      minCoin: 1
    },
    rating: {
      minPoint: 1,
      maxPoint: 5,
      averagePoint: 3,
      zcoin: 20,
      feedLength: 40
    },
    verifyPhone: {
      codeLength: 6
    },
    linkCard: {
      codeLength: 7
    },
    voucher: {
      minCoin: 1,
      minQuantity: 0
    },
    event: {
      minCoin: 1,
      minMilestone: 1,
      minMultiplier: 1,
      minBillStackAmount: 1,
      minBillStackCoin: 0,
      minBillStackDiscount: 0
    },
    spin: {
      minTicket: 1,
      minPrice: 0
    },
    prize: {
      minPercent: 0,
      minCoin: 0
    },
    mayor: {
      leastCheckin: 3,
      coin: 50,
      minCheckin: 1,
      minCoin: 0
    },
    luckyCheckin: {
      minCheckin: 1,
      minCoin: 1
    },
    quest: {
      minCoin: 0
    },
    bill: {
      minPrice: 0
    },
    merchant: {
      minNormalFrom: 0,
      minNormalTo: 0,
      minVipFrom: 0,
      minVipTo: 0,
      minLostDays: 1
    },
    chain: {
      minNormalFrom: 0,
      minNormalTo: 0,
      minVipFrom: 0,
      minVipTo: 0,
      minLostDays: 1
    },
    campaign: {
      minAge: 0,
      quantity: 1,
      smsFee: 700,
      byCustomerLevel: 4,
      sendMessageDelay: 500
    },
    luckyDraw: {
      joiningZcoin: 300,
      consolationPrizeZcoin: 30
    },
    promotionCode: {
      length: 6
    }
  }
}
