const uploadDir = process.cwd() + '/uploads/'
const thumbnailPrefix = 'sm_'
const mediumPrefix = 'md_'

export default {
  // file
  file: {
    uploadDir,
    minQuality: 90,
    removeImageTimeout: 0,
    resizeTypes: {
      avatar: 'avatar',
      banner: 'banner',
      business: 'business',
      category: 'category',
      chat: 'chat',
      cover: 'cover', // Reward, Spin, Prize
      dish: 'dish',
      sticker: 'sticker',
      voucher: 'voucher',
      icon: 'icon',
      logo: 'logo'
    },
    resizes: {
      avatar: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 400,
        height: 400,
        gravity: 'center',
        extent: [400, 400],
        isPublic: true
      }],
      banner: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 800,
        height: 500,
        gravity: 'center',
        extent: [800, 500],
        isPublic: true
      }],
      cover: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 800,
        height: 500,
        gravity: 'center',
        extent: [800, 500],
        isPublic: true
      }],
      chat: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 800,
        height: 500,
        gravity: 'center',
        extent: [800, 500],
        isPublic: false
      }],
      category: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 360,
        height: 360,
        gravity: 'center',
        extent: [360, 360],
        isPublic: true
      }],
      dish: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 360,
        height: 360,
        gravity: 'center',
        extent: [360, 360],
        isPublic: true
      }],
      business: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 800,
        height: 500,
        gravity: 'center',
        extent: [800, 500],
        isPublic: true
      }, {
        prefix: mediumPrefix,
        quality: 0.95,
        width: 1280,
        height: 800,
        gravity: 'center',
        extent: [1280, 800],
        isPublic: true
      }],
      sticker: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 400,
        height: 400,
        gravity: 'center',
        extent: [400, 400],
        isPublic: true
      }],
      voucher: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 800,
        height: 500,
        gravity: 'center',
        extent: [800, 500],
        isPublic: true
      }],
      icon: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 150,
        height: 150,
        gravity: 'center',
        extent: [150, 150],
        isPublic: true,
        absolutely: true
      }],
      logo: [{
        prefix: thumbnailPrefix,
        quality: 0.95,
        width: 450,
        height: 450,
        gravity: 'center',
        extent: [450, 450],
        isPublic: true,
        absolutely: true
      }]
    }
  },

  // Validate file
	validateFile: {
    type: {
      photo: 'photo',
      zip: 'zip',
      excel: 'excel'
    },
    photo: {
      text: 'hình ảnh',
      maxSize: 10000000, // 10MB
      textMaxSize: '10MB',
      extensions: /\.(jpe?g|png|gif)$/i,
      textExtensions: '.jpg/.jpeg/.png/.gif'
    },
    zip: {
      text: 'tập tin',
      maxSize: 5000000, // 5MB
      textMaxSize: '5MB',
      extensions: /\.(zip)$/i,
      textExtensions: '.zip'
    },
    excel: {
      text: 'tập tin',
      maxSize: 500000, // 0.5MB
      textMaxSize: '0.5MB',
      extensions: /\.(xlsx?)$/i,
      textExtensions: '.xls/.xlsx'
    }
  }
}
