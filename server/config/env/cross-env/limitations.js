export default {
  // Limit when query
  limit: {
    activity: {
      business: 3 // Business detail: recent activites
    },
    business: {
      search: 15, // Home, map
      all: 20, // Admin page
      photo: 15, // Photo
      trending: 20
    },
    chat: {
      history: 15,
      messageByRoom: 20
    },
    user: {
      activity: 15, // Profile activity
      topCheckinPlaces: 3, // Profile: top checkin places
      search: 50, // Admin search
      reward: 10, // App reward history
      spin: 20, // App spin history
      chat: 15, // Recent chat history
      message: 20, // List messages
      zcoinHistory: 20, // For admin analytic
      voucherHistory: 20, // For admin analytic
      voucherUnused: 20, // For admin analytic
      suspicion: 20
    },
    chain: {
      search: 10,
      all: 20, // All chains
      activities: 50, // Activities page
      customers: 50, // Customers page
      customerActivities: 20 // Customer activities
    },
    voucher: {
      all: 10, // All vouchers,
      related: 9, // Related vouchers
      onboarding: 20, // Onboarding page
      home: 10
    },
    voucherGroup: {
      all: 10
    },
    luckyCheckin: {
      all: 20 // All lucky checkin programs
    },
    quest: {
      all: 20 // All quests
    },
    category: {
      all: 20 // All categories
    },
    bill: {
      merchantAll: 20 // All bills for merchant page
    },
    purpose: {
      all: 20 // All purposes
    },
    cuisine: {
      all: 20 // All cuisines
    },
    merchant: {
      topExpense: 3, // Top expense users of merchant
      customers: 20, // All customers
      feedback: 20, // All feedback
      reward: 20, // All rewards
      activities: 20, // All activities
      activities2_7: 50,
      rewards2_7: 50,
      topExpense2_7: 10,
      topTransaction2_7: 10,
      listBirthday2_7: 10,
      chatHistory2_7: 20,
      allCampaigns2_7: 10,
      searchChat2_7: 10
    },
    leaderboard: {
      user: 30 // Total users will display on leadearboard
    },
    banner: {
      all: 20 // Admin page
    },
    voucherCategory: {
      all: 20 // Admin page
    },
    collection: {
      search: 5, // Search businesses
      all: 20 // Admin page
    },
    home: {
      voucher: 9, // Home voucher
      campaign: 9
    },
    analytic: {
      bill: 20, // Bill analytic
      user: 50, // All users
      voucher: 50, // Voucher analytic
      sms: 50, // SMS logs
      campaign: 50, // Done campaigns
      campaignList: 50, // Campaign list data
      deleteBills : 30, // Number deletedbills
      userReferrals : 30 // Number user referrals
    },
    admin: {
      searchUser: 5, // Search user
      searchBusinessByName: 5, // Search business by name
      searchVoucherByName: 5, // Search voucher by name
      searchLuckyDrawByName: 5 // Search lucky draw by name
    },
    account: {
      all: 20 // All merchants account
    },
    campaign: {
      index: 20,
      relativeActive: 10,
      home: 10,
      all: 20,
      histories: 20,
      redeemed: 20
    },
    feedback: {
      stars: {
        min: 1,
        max: 5
      }
    },
    luckyDraw: {
      todayWinners: 10,
      history: 10
    },
    prize: {
      home: 10
    },
    campaignAutomation: {
      adminAll: 20, // Admin all
      all: 3, // All for each type
      allByType: 10, // By type
      transactions: 20, // List transactions
      redeemed: 20,  // List redeemed
      all2_7: 10
    },
    campaignManual: {
      adminAll: 20, // Admin all
      all: 3, // All for each type
      allByType: 10, // By type
      transactions: 20, // List transactions
      redeemed: 20,  // List redeemed
      all2_7: 10
    },
    campaignAutomationV3: {
      adminAll: 20, // Admin all
      all: 10, // All for each type
      allByType: 10, // By type
      transactions: 20, // List transactions
      redeemed: 20,  // List redeemed
      sent: 20 // List messages sent
    },
    newsFeed: {
      index: 20,
      ratings: 20
    },
    affiliate: {
      all: 20
    },
    bank: {
      index: 20
    },
    product: {
      index: 20
    },
    onlineCategory: {
      index: 10
    },
    code: {
      index: 20
    },
    news: {
      index: 20
    },
    comment: {
      index: 10
    }
  }
}
