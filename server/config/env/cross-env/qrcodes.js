export default {
  qrCodeCover: {
    ext: 'jpg',
    margin: 1, // white space around QR image in modules. Default 4 for png and 1 for others.
    size: 20, // size (png and svg only) — size of one module in pixels. Default 5 for png and undefined for svg
    bgWidth: 600, // background width
    bgHeight: 300, // background height
    bgColor: '#f2f2f2', // background color
    bgPrefix: 'qr_', // background prefix
    geometry: '+150+0',
    width: 300,
    height: 300,
    filter: '!'
  }
}