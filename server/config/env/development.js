module.exports = {
  db: {
    fullPath: 'mongodb://127.0.0.1/zodyapp-dev',
    options: (isIndexesServer = true) => {
      return {
        useMongoClient: true,
        config: {
          autoIndex: isIndexesServer
        },
        poolSize: 10,
        autoReconnect: true,
        keepAlive: 30000,
        socketTimeoutMS: 30000,
        connectTimeoutMS: 30000,
        w: 'majority',
        wtimeout: 10000,
        j: true,
        native_parser: false,
        ha: true,
        haInterval: 10000
      }
    },
  },
  S3: {
    host: 'https://zodyapp-dev.s3.amazonaws.com/',
    bucket: 'zodyapp-dev'
  },
  secret: '8?@B##o!fV}5R8G'
}