const env = process.env.NODE_ENV || 'development'
const config = require(`./env/${env}`)
const defaults = require('./env').default
export default Object.assign(defaults, config)