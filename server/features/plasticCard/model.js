import { mongoose, Schema } from '../../util/mongoose'
import Statics from './static'

const PlasticCardSchema = new Schema({
  serial: {
    type: String,
    required: true
  },
  tag: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  phone: String,
  linkedAt: Date,
  linkedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
})

PlasticCardSchema.index({ phone: 1 }).index({ user: 1 }).index({ serial: 1 }).index({ tag: 1 })

PlasticCardSchema.statics = Statics

export default mongoose.model('PlasticCard', PlasticCardSchema)
