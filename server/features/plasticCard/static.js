import {ObjectId} from '../../util/mongoose'
import config from '../../config'
import PlasticCard from './model'

/**
 * Get user card info
 *
 * @param {ObjectId} userId
 * @param {String}   phone
 */
async function info(userId, phone) {
  if (!userId && !phone) {
    return config.conventions.object
  }

  const condition = {}
  if (userId) {
    condition.user = new ObjectId(userId)
  } else if (phone) {
    condition.phone = phone
  }

  let card = await PlasticCard.findOne(condition).select('linkedAt linkedBy serial')
    .populate('linkedBy', 'name').lean().exec()
  if (!card) {
    return config.conventions.object
  }
  card.canUnlink = (Date.now() - (new Date(card.linkedAt)).getTime() < config.plasticCard.removeExpireTime)
  console.log('card', card)
  return card
}

export default {
  info
}
