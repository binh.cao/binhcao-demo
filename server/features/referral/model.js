import config from '../../config'
import {mongoose, Schema} from '../../util/mongoose'
import otherCode from '../../locales/other'
// import Statics from './static'

let ReferralSchema = new Schema({
  user: {
    required: otherCode.userRequired.code.toString(),
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  code: {
    type: String,
    required: otherCode.referral.codeRequired.code.toString(),
    minlength: [config.strict.referral.minLength, otherCode.referral.codeMinLength.code.toString()],
    maxlength: [config.strict.referral.maxLength, otherCode.referral.codeMaxLength.code.toString()]
  },
  modifyTimes: {
    type: Number,
    default: 0,
    max: [config.referral.modifyTimes, otherCode.referral.modifyTimes.code.toString()]
  },
  referrals: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  referralBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  deviceInformation: {
    deviceId: String,
    deviceOS: String
  },
  referralAt: Date,
  createdAt: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
})

ReferralSchema.index({
  referralBy: 1
})

ReferralSchema.index({
  user: 1
})

// ReferralSchema.statics = Statics

export default mongoose.model('Referral', ReferralSchema)
