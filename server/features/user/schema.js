const types = `
  type User {
    _id: ID!
    name: String!
    email: String!
    phone: String!
    city: String
    avatar: String
    birthday: String
    gender: String
    facebook: String
    business: ID
  }
  
  type SignInPayload {
    token: String
    refreshToken: String
    user: User
  }
  
  input AUTH_PROVIDER_EMAIL {
    email: String!
    password: String!
  }
`
const queries = `
  allUsers: [User!]!
`

const mutations = `
  signIn(email: String!, password: String!): SignInPayload!
`

export default {
  types,
  queries,
  mutations
}