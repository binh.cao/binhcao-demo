import User from './model'
import StringHelper from '../../util/stringHelper'
import commonCode from '../../locales/common'
import config from '../../config/env/index'
import TokenGenerator from '../../util/tokenGenerator'
import ValidationError from '../../util/error/validationError'

const queries = {
  allUsers: async () => {
    return await User.find({})
  }
}

const mutations = {
  signIn: async (root, data) => {
    let {phone, email, password} = data

    let isNew

    if (email) {
      email = email.toLowerCase()
    }

    phone = StringHelper.formatPhoneNumber(phone)

    let condition = []
    if (phone) {
      condition.push({phone})
    }
    if (email) {
      condition.push({email})
    }

    let user = await User.findOne({
      $or: condition
    })
    if (!user) {
      throw new ValidationError(commonCode.authenticateFailed)
    }
    isNew = user.isNewUser
    if (process.env.NODE_ENV === config.env.production) {
      if (!user.authenticate(password)) {
        throw new ValidationError(commonCode.authenticateFailed)
      }
    }

    if (user.roles.indexOf(config.user.roles.sale) > -1) {
      user._id = global.adminId
      user.name = 'Zody - Hỗ trợ'
    }
    return {
      user,
      token: TokenGenerator.generate(user),
      refreshToken: TokenGenerator.generate(user, {expiresIn: '30d'})
    }
  }
}

const data = {
  User: {
    city: root => root.city || config.city.daNang,
    avatar: root => User.avatar(root),
    gender: root => root.gender || config.gender.male,
    facebook: root => {
      if (root.facebook && root.facebook.profileUrl) {
        return root.facebook.profileUrl
      } else {
        return config.conventions.string
      }
    }
  }
}

export default {
  queries,
  mutations,
  data
}