import PhotoHelper from '../../util/photoHelper'

/**
 * Get user avatar, priority: local > facebook > no avatar
 *
 * @param  {Object} data: user data
 * @return {String}
 */
function avatar(data) {
	// If has app avatar, use it
	if (data.avatar) {
		return PhotoHelper.getAvatarUrl(data.avatar)
	}
	// Else find for facebook avatar
	else if (data && data.facebook && data.facebook.photos) {
		return data.facebook.photos
	}
	// Else use default
	else {
		return PhotoHelper.defaultAvatar()
	}
}

export default {
	avatar
}
