import config from '../../config'
import {mongoose, Schema} from '../../util/mongoose'
import otherCode from '../../locales/other'
// import Hooks from './hook';
// import Statics from './static'
// import Methods from './method'

const BillSchema = new Schema({
  // User
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },

  // Business
  business: {
    type: Schema.ObjectId,
    ref: 'Business',
    required: otherCode.businessRequired.code.toString()
  },

  campaigns: [{
    type: Schema.ObjectId,
    ref: 'CampaignManual'
  }],

  campaignAutomations: [{
    type: Schema.ObjectId,
    ref: 'CampaignAutomation'
  }],

  campaignAutomationV3: [{
    type: Schema.ObjectId,
    ref: 'CampaignAutomationV3'
  }],

  // Chain
  chain: {
    type: Schema.ObjectId,
    ref: 'Chain'
  },

  // This field is using for brand, ref brand bill to original bill id
  fromBill: {
    type: Schema.ObjectId,
    ref: 'Bill'
  },

  // Staff who created this bill
  staff: {
    type: Schema.ObjectId,
    ref: 'User'
  },

  // Is first bill flag
  isFirstBill: {
    type: Boolean,
    default: false
  },

  // Bill price
  price: {
    type: Number,
    min: [config.strict.bill.minPrice, otherCode.bill.minPrice.code.toString()]
  },

  // Custom bill id from merchant
  billId: String,

  // User phone number
  phone: String,

  // Coin receive from this bill
  coin: {
    type: Number,
    default: 0
  },

  // Discount percent
  cashDiscount: {
    type: Number,
    default: 0
  },

  // Bonus discount percent, cashier add manually
  bonusDiscount: {
    type: Number,
    default: 0
  },

  // City of this bill
  city: String,

  // Source of bill
  source: String,

  // Items of bill
  items: [String],

  isWinback: Boolean,

  // Note for merchant
  note: String,

  // Identify who deleted this bill
  deletedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  statuses: {
    suspicion: {
      type: String,
      enum: config.user.suspicion.reasons.all.split(' ')
    }
  },

  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date
}, {
  versionKey: false
})

BillSchema.index({business: 1}).index({user: 1}).index({chain: 1, user: 1})
  .index({campaignAutomations: 1}).index({campaigns: 1})

// BillSchema.methods = Methods
// BillSchema.statics = Statics

// BillSchema.pre('save', function (next) {
//   Hooks.preSaveHook(this, next)
// })

// BillSchema.post('save', function (doc) {
//   Hooks.postSaveHook(doc)
// })

// BillSchema.post('remove', function (doc) {
//   Hooks.postRemoveHook(doc)
// })

// Export
export default mongoose.model('Bill', BillSchema)
