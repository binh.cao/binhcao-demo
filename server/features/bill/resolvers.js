import {ObjectId} from '../../util/mongoose/index'
import Bill from './model'
import config from '../../config'
import Referral from '../referral/model'
import PlasticCard from '../plasticCard/model'
import MiddleWare from '../../util/middleware'

const queries = {
  allBillsInBusiness: async (root, data, {user}) => {
    MiddleWare.requiresStaffOrBusiness(user)
    let {page, sort = '-createdAt'} = data
    let businessId = new ObjectId(user.business)
    const total = await Bill.count({
      business: businessId
    })
    const bills = await Bill.find({
      business: businessId
    })
      .sort(sort)
      .skip(page * config.limit.bill.merchantAll)
      .limit(config.limit.bill.merchantAll)
      .populate('user', 'name phone integratedId')
      .populate('cashier', 'name')
      .lean()
      .exec()
    return {
      total,
      limitPerPage: config.limit.bill.merchantAll,
      data: bills
    }
  }
}

const mutations = {}

const data = {
  Bill: {
    userId: async (bill) => {
      if (bill.user && bill.user._id) {
        const referralCode = await Referral.findOne({
          user: new ObjectId(bill.user._id)
        })
          .select('code')
          .lean()
          .exec()
        return referralCode ? referralCode.code : config.conventions.string
      }
    },
    plasticCard: async (bill) => {
      return await PlasticCard.info(bill.user ? bill.user._id : '', bill.phone)
    },
    user: bill => bill.user || config.conventions.object,
    canRemove: bill => (Date.now() - (new Date(bill.createdAt)).getTime() < config.bill.removeExpireTime)
  }
}

export default {
  queries,
  mutations,
  data
}

