const types = `
  type Bill {
    _id: ID!
    user: User
    business: ID
    userId: String
    plasticCard: String
    canRemove: Boolean
    limitPerPage: Int
    fromBill: String
    staff: User
    isFirstBill: Boolean
    price: Int
  }
  
  type INDEX_BILL_PAYLOAD {
    total: Int
    limitPerPage: Int
    data: [Bill!]!
  }
`
const queries = `
  allBillsInBusiness(page: Int, sort: String): INDEX_BILL_PAYLOAD!
`

const mutations = `
`

export default {
  types,
  queries,
  mutations
}
