import Authenticator from './util/authenticator'
import {graphqlExpress, graphiqlExpress} from 'apollo-server-express'
import bodyParser from 'body-parser'
import buildDataLoaders from './dataloaders'
import dbConnector from './mongo-connector'
import express from 'express'
import formatError from './util/error/formatError'
import morgan from 'morgan'
import schema from './schema.js'

async function start() {
  await dbConnector.connect()
  const app = express()
  const buildOptions = async (req, res) => {
    const user = await Authenticator.authenticate(req)
    return {
      context: {
        // dataloaders: buildDataLoaders(mongo),
        // mongo,
        user
      },
      formatError,
      schema
    }
  }
  app.use(morgan('dev'))
  app.use('/graphql', bodyParser.json(), graphqlExpress(buildOptions))
  app.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
    passHeader: `'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODU0YTZlYTc2MGFhYzhhNmVlYjIwYjYiLCJidXNpbmVzcyI6IjU3YmE4MDNmOTJkMGZlNzkyNDdiMzdlYSIsIm5hbWUiOiJab2R5IMSQw6AgTuG6tW5nIiwic2VhcmNoU3RyaW5nIjoiem9keSBkYSBuYW5nIiwiYXZhdGFyIjoiODc0NjU0NTM5NzExXzE0NzIwMTAxMjY0MzkuanBnIiwiZW1haWwiOiJ6b2R5Y2FzaGllckB6b2R5YXBwLmNvbSIsImhhc2hlZF9wYXNzd29yZCI6Iit3V2V6aUp6YS9SWHU0T0g4MnZuMENhK2Y2djdBMkNVeElJNzJjRkRTaHBHNlpRc1hwd3ljZ25qb2w0TUVtSnF3a2dTRnhYSysxRXpHVTZhbTBFOU1BPT0iLCJzYWx0IjoiTU10OTkzUXRxd01vVXA2US83RkV6UT09IiwibGFzdEFjdGl2ZWRBdCI6IjIwMTctMTEtMjNUMDg6Mzc6MTYuMTI0WiIsInZlcnNpb24iOiIxLjAuMiIsImJpcnRoZGF5IjoiMTk5MS0wMi0wOFQyMzoyMzoyMy4wMDBaIiwicGhvbmUiOiIiLCJsYXN0VXBkYXRlUGFzc3dvcmRBdCI6IjIwMTctMDYtMzBUMDc6MDE6NTQuMjkwWiIsImlzTmV3VXNlciI6ZmFsc2UsInVwZGF0ZWRBdCI6IjIwMTctMDMtMjBUMTY6Mzk6MzUuNTI0WiIsImNyZWF0ZWRBdCI6IjIwMTYtMTItMTdUMDI6NDY6MDIuMjk0WiIsInF1ZXN0cyI6WyI1N2Q5MWEzMmY5ODQwNmNmMjExMGI5NjIiLCI1N2RhMGIzYmY5ODQwNmNmMjExMGJiMDgiLCI1N2RhMGI2ODFmZDAxZjZjNWM5MjgxNjgiLCI1N2RhMGI5ODFmZDAxZjZjNWM5MjgxNjkiLCI1N2RhMGJlM2Y5ODQwNmNmMjExMGJiMGEiLCI1N2RhMGMzNzQ0ZTBlN2NlMjFlZmI1OWIiLCI1N2RhMGM2ZGY5ODQwNmNmMjExMGJiMGIiLCI1N2RhMGNhNTFkNzNhZTcxNWNhNzhiZGYiLCI1N2RhMGNmMmY5ODQwNmNmMjExMGJiMGMiLCI1N2RhMGQxMzFkNzNhZTcxNWNhNzhiZTAiLCI1N2RhMGQzMDFmZDAxZjZjNWM5MjgxNmIiLCI1N2RhMGQzMzc3ZWEwMDM3M2Q5OGMwOTciLCI1N2RhMGRlODQ0ZTBlN2NlMjFlZmI1YTEiLCI1N2RhMGRlOTg3NjVhYjM4M2Q2YmQxNzciLCI1N2RhMGU1NzFkNzNhZTcxNWNhNzhiZTEiLCI1N2RhMGU4NjFkNzNhZTcxNWNhNzhiZTIiLCI1N2RhMGViZjFmZDAxZjZjNWM5MjgxNmQiLCI1N2RhMGVmMTFkNzNhZTcxNWNhNzhiZTQiLCI1N2RhMGY1OGY5ODQwNmNmMjExMGJiMTciLCI1N2RhMGY2NjFmZDAxZjZjNWM5MjgxNmUiXSwic3RhdHVzZXMiOnsiYmFubmVkIjpmYWxzZSwidmVyaWZpZWQiOmZhbHNlLCJvbmxpbmUiOmZhbHNlfSwic3RhdGlzdGljIjp7Imxhc3RCaWxsQXQiOm51bGwsImxhc3RDaGVja2luQXQiOiIyMDE3LTAzLTIwVDIwOjQ4OjE3LjE0OVoiLCJiYWRnZSI6MCwicmV3YXJkIjowLCJleHBlbnNlIjowLCJiaWxsZWRaY29pbiI6MCwiY2hlY2tpbkNvaW4iOjI1LCJjb2luIjo3NSwiYmlsbCI6MCwiY2hlY2tpbiI6MX0sInJvbGVzIjpbInN0YWZmIiwiYXV0aGVudGljYXRlZCIsInN0YWZmIiwic3RhZmYiXSwiZ2VuZGVyIjoibWFsZSIsImNpdHkiOiJkYS1uYW5nIiwibG9jYWxlIjoidmkiLCJpYXQiOjE1MTUwNDA0MTEsImV4cCI6MTUxNzYzMjQxMX0.ZEgFz3qdukPWbjtgULf6p4VFtUY_Z2cmKdaDOoSO-rA'`
  }))
  require('./init')
  const PORT = 3000
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}.`)
  })
}

start()
