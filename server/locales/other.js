/**
 * Other components code, from 1000-1999
 */

import config from '../config';

export default {
  // Required target
  userRequired: {
    code: 1000,
    message: 'User id không được trống'
  },
  businessRequired: {
    code: 1001,
    message: 'Business id không được trống'
  },
  eventRequired: {
    code: 1002,
    message: 'Event id không được trống'
  },
  roomRequired: {
    code: 1003,
    message: 'Room id không được trống'
  },
  menuRequired: {
    code: 1004,
    message: 'Menu id không được trống'
  },
  spinRequired: {
    code: 1005,
    message: 'Spin id không được trống'
  },
  questRequired: {
    code: 1006,
    message: 'Quest id không được trống'
  },
  chainRequired: {
    code: 1007,
    message: 'Chain id không được trống'
  },
  localeCodeRequired: {
    code: 1008,
    message: 'Mã ngôn ngữ không được trống'
  },
  campaignRequired: {
    code: 1009,
    message: 'Campaign id không được trống'
  },
  luckyDrawRequired: {
    code: 1010,
    message: 'LuckyDraw không được trống'
  },
  prizeRequired: {
    code: 1011,
    message: 'Prize không được trống'
  },
  prizeCategoryRequired: {
    code: 1012,
    message: 'Prize Category không được trống'
  },
  mobileNetworkRequired: {
    code: 1013,
    message: 'Nhà mạng không được trống'
  },
  productRequired: {
    code: 1014,
    message: 'Sản phẩm không được trống'
  },

  // Referral
  referral: {
    modifyTimes: {
      code: 1050,
      message: `Bạn chỉ có thể đổi mã giới thiệu ${ config.referral.modifyTimes } lần` // 1
    },
    codeRequired: {
      code: 1051,
      message: 'Mã giới thiệu không được trống'
    },
    codeMinLength: {
      code: 1052,
      message: `Mã giới thiệu phải có ít nhất ${ config.strict.referral.minLength } ký tự` // 6
    },
    codeMaxLength: {
      code: 1053,
      message: `Mã giới thiệu chỉ được tối đa ${ config.strict.referral.maxLength } ký tự` // 10
    },
    invalidCode: {
      code: 1054,
      message: 'Mã giới thiệu không chính xác, vui lòng kiểm tra lại'
    },
    fullOfInvitation: {
      code: 1055,
      message: `Người dùng này đã đạt giới hạn ${ config.referral.max } mời bạn`
    },
    alreadyInputedReferralCode: {
      code: 1056,
      message: 'Bạn đã nhập mã giới thiệu trước đây'
    },
    deviceInvalid: {
      code: 1057,
      message: 'Thiết bị này đã tham gia chương trình mời bạn trước đây'
    },
    updateVersionNeeded: {
      code: 1056,
      message: 'Vui lòng cập nhật ứng dụng lên phiên bản mới nhất để sử dụng tính năng này'
    },
    oneCodePerDevice: {
      code: 1057,
      message: 'Mỗi thiết bị chỉ nhập mã mời bạn 1 lần duy nhất. Vui lòng liên hệ Zody để được tư vấn!'
    }
  },

  // Zcoin
  zcoin: {
    coinRequired: {
      code: 1150,
      message: 'Coin tặng thưởng không được trống'
    },
    minCoin: {
      code: 1151,
      message: `Coin tặng thưởng không được nhỏ hơn ${ config.strict.zcoin.minCoin }` // 1
    },
    invalidType: {
      code: 1152,
      message: 'Loại tặng thưởng không hợp lệ'
    }
  },

  // Chain store
  chain: {
    nameRequired: {
      code: 1250,
      message: 'Tên chuỗi địa điểm không được trống'
    },
    minNormalFrom: {
      code: 1251,
      message: `Chi tiêu tối thiểu của thành viên thường không được nhỏ hơn ${config.strict.chain.minNormalFrom}`
    },
    minNormalTo: {
      code: 1252,
      message: `Chi tiêu tối đa của thành viên thường không được nhỏ hơn ${config.strict.chain.minNormalTo}`
    },
    minVipFrom: {
      code: 1253,
      message: `Chi tiêu tối thiểu của thành viên VIP không được nhỏ hơn ${config.strict.chain.minVipFrom}`
    },
    minVipTo: {
      code: 1254,
      message: `Chi tiêu tối đa của thành viên VIP không được nhỏ hơn ${config.strict.chain.minVipTo}`
    },
    minLostDays: {
      code: 1255,
      message: `Số ngày không có giao dịch của thành viên không được nhỏ hơn ${config.strict.chain.minLostDays}`
    }
  },

  // Rating
  rating: {
    minPoint: {
      code: 1300,
      message: `Điểm đánh giá không được nhỏ hơn ${ config.strict.rating.minPoint }` // 1
    },
    maxPoint: {
      code: 1301,
      message: `Điểm đánh giá không được lớn hơn ${ config.strict.rating.maxPoint }` // 5
    }
  },

  // Event
  event: {
    minCoin: {
      code: 1350,
      message: `Điểm nhận được mỗi lần checkin không được nhỏ hơn ${ config.strict.event.minCoin }` // 1
    },
    minMilestone: {
      code: 1351,
      message: `Mốc checkin không được nhỏ hơn ${ config.strict.event.minMilestone }` // 1
    },
    minMultiplier: {
      code: 1352,
      message: `Zcoin được nhân không được nhỏ hơn ${ config.strict.event.minMultiplier }` // 1
    },
    minBillStackAmount: {
      code: 1353,
      message: `Số tiền tích luỹ theo hoá đơn không được nhỏ hơn ${ config.strict.event.minBillStackAmount }` // 1
    },
    minBillStackCoin: {
      code: 1354,
      message: `Số zcoin tích luỹ khi đạt mốc không được nhỏ hơn ${ config.strict.event.minCoin }` // 0
    },
    // minBillStackDiscount: {
    //   code: 1355,
    //   message: `Số % tích luỹ cộng thêm khi đạt mốc không được nhỏ hơn ${ config.strict.event.minDiscount }` // 0
    // }
  },

  // Beacon
  beacon: {
    majorRequired: {
      code: 1400,
      message: 'Số major không được trống'
    },
    minorRequired: {
      code: 1401,
      message: 'Số minor không được trống'
    },
    belongToOtherBusiness: {
      code: 1402,
      message: 'Beacon này đã được tạo cho địa điểm khác'
    }
  },

  voucherGroup: {
    nameRequired: {
      code: 1449,
      message: 'Tên nhóm voucher không được trống'
    }
  },

  // Category
  category: {
    nameRequired: {
      code: 1450,
      message: 'Tên danh mục không được trống'
    }
  },

  // Cuisine
  cuisine: {
    nameRequired: {
      code: 1451,
      message: 'Tên ẩm thực không được trống'
    }
  },

  // Purpose
  purpose: {
    nameRequired: {
      code: 1452,
      message: 'Tên mục đích không được trống'
    }
  },

  // Message
  message: {
    invalidType: {
      code: 1500,
      message: 'Loại tin nhắn không hợp lệ'
    }
  },

  // Verify phone
  verifyPhone: {
    codeRequired: {
      code: 1550,
      message: 'Mã xác nhận số điện thoại không được trống'
    },
    phoneRequired: {
      code: 1551,
      message: 'Số điện thoại không được trống'
    },
    expiredCode: {
      code: 1552,
      message: 'Mã xác nhận không tìm thấy hoặc đã hết hạn, vui lòng yêu cầu mã xác nhận mới'
    }
  },

  // Menu
  menu: {
    nameRequired: {
      code: 1600,
      message: 'Tên danh mục không được trống'
    }
  },

  // Dish
  dish: {
    nameRequired: {
      code: 1601,
      message: 'Tên món không được trống'
    }
  },

  // Photo
  photo: {
    nameRequired: {
      code: 1650,
      message: 'Tên hình ảnh không được trống'
    }
  },

  // Voucher
  voucher: {
    coinRequired: {
      code: 1700,
      message: 'Zcoin để đổi voucher không được trống'
    },
    minCoin: {
      code: 1701,
      message: `Zcoin để đổi voucher không được nhỏ hơn ${ config.strict.voucher.minCoin }` // 1
    },
    quantityRequired: {
      code: 1702,
      message: 'Số lượng voucher không được trống'
    },
    minQuantity: {
      code: 1703,
      message: `Số lượng không được nhỏ hơn ${ config.strict.voucher.minQuantity }` // 0
    },
    nameRequired: {
      code: 1704,
      message: 'Tên voucher không được trống'
    },
    viNameRequired: {
      code: 1705,
      message: 'Tên tiếng Việt không được trống'
    },
    enNameRequired: {
      code: 1706,
      message: 'Tên tiếng Anh không được trống'
    },
    outOfStock: {
      code: 1707,
      message: 'Số lượng voucher này đã hết'
    },
    notEnoughCoin: {
      code: 1708,
      message: 'Bạn không đủ zcoin để đổi voucher này'
    },
    alreadyExchanged: {
      code: 1709,
      message: 'Bạn đã đổi quà tặng này rồi, vui lòng sử dụng trước khi đổi quà tặng mới'
    },
    voucherCanOnlyExchangeOnce: {
      code: 1710,
      message: 'Voucher này chỉ được đổi 1 lần duy nhất'
    },
    expired: {
      code: 1711,
      message: 'Voucher này đã hết hạn'
    },
    cannotUpdateCategoriesOnChildVoucher: {
      code: 1713,
      message: 'Không thể cập nhật categories'
    },
    groupInValid: {
      code: 1714,
      message: 'Voucher group không tồn tại'
    },
    groupNameInValid: {
      code: 1715,
      message: 'groupName và groupDisplayName không được trống'
    },
    deactived: {
      code: 1716,
      message: 'Voucher này hiện không thể đổi được, vui lòng liên hệ "Zody - Hỗ trợ" để biết thêm chi tiết'
    },
    ticketBoxNotFound: {
      code: 1717,
      message: 'Vé ticketBox không tìm thấy'
    },
    processingAnotherRequest: {
      code: 1718,
      message: 'Bạn đang đổi voucher từ thiết bị khác'
    }
  },

  // Spin
  spin: {
    nameRequired: {
      code: 1750,
      message: 'Tên spin game không được trống'
    },
    viNameRequired: {
      code: 1751,
      message: 'Tên tiếng Việt không được trống'
    },
    enNameRequired: {
      code: 1752,
      message: 'Tên tiếng Anh không được trống'
    },
    minTicket: {
      code: 1753,
      message: `Lượt quay bonus của địa điểm không được nhỏ hơn ${ config.strict.spin.minTicket }` // 1
    },
    minPrice: {
      code: 1754,
      message: `Giá mua lượt quay không được nhỏ hơn ${ config.strict.spin.minPrice }` // 0
    },
    cityRequired: {
      code: 1755,
      message: 'Thành phố không được trống'
    },
    invalidApplyBusinesses: {
      code: 1756,
      message: 'Vui lòng chọn địa điểm áp dụng tặng lượt quay'
    },
    notEnoughCoinToBuyTicket: {
      code: 1757,
      message: 'Bạn không đủ Zcoin để mua lượt quay'
    },
    notActive: {
      code: 1758,
      message: 'Vòng quay may mắn không tồn tại'
    },
    expired: {
      code: 1759,
      message: 'Vòng quay may mắn đã hết hạn'
    },
    notEnoughTicket: {
      code: 1760,
      message: 'Bạn đã hết lượt quay, vui lòng mua thêm lượt'
    },
    hasNoPrize: {
      code: 1761,
      message: 'Vòng quay may mắn hiện tại chưa có giải thưởng nào'
    }
  },

  // Prize
  prize: {
    nameRequired: {
      code: 1800,
      message: 'Tên giải thưởng không được trống'
    },
    percentRequired: {
      code: 1803,
      message: 'Phần trăm trúng thưởng không được trống'
    },
    minPercent: {
      code: 1804,
      message: `Phần trăm trúng thưởng không được nhỏ hơn ${ config.strict.prize.minPercent }` // 0
    },
    invalidType: {
      code: 1805,
      message: 'Loại tặng thưởng không hợp lệ'
    },
    minCoin: {
      code: 1806,
      message: `Zcoin trúng thưởng không được nhỏ hơn ${ config.strict.prize.minCoin }` // 0
    }
  },

  // Mayor
  mayor: {
    minCheckin: {
      code: 1850,
      message: `Số lần checkin để đạt thị trưởng không được nhỏ hơn ${ config.strict.mayor.minCheckin }` // 1
    },
    minCoin: {
      code: 1851,
      message: `Zcoin tặng thị trưởng không được nhỏ hơn ${ config.strict.mayor.minCoin }` // 0
    },
    invalidStatus: {
      code: 1852,
      message: 'Trạng thái thị trưởng không hợp lệ'
    }
  },

  // Lucky checkin
  luckyCheckin: {
    checkinMilestoneRequired: {
      code: 1900,
      message: 'Mốc checkin không được trống'
    },
    minCheckin: {
      code: 1901,
      message: `Mốc checkin không được nhỏ hơn ${ config.strict.luckyCheckin.minCheckin }`
    },
    minCoin: {
      code: 1902,
      message: `Zcoin thưởng không được nhỏ hơn ${ config.strict.luckyCheckin.minCoin }`
    },
    invalidType: {
      code: 1903,
      message: 'Loại checkin may mắn không hợp lệ'
    }
  },

  // Reward
  reward: {
    invalidType: {
      code: 1950,
      message: 'Loại phần thưởng không hợp lệ'
    },
    used: {
      code: 1951,
      message: 'Phần thưởng này đã được sử dụng trước đó'
    },
    invalidReward: {
      code: 1952,
      message: 'Phần thưởng này không tồn tại, vui lòng kiểm tra lại'
    },
    invalidForBonus: {
      code: 1953,
      message: 'Không thể tặng phần thưởng này.'
    }
  },

  // Quest
  quest: {
    nameRequired: {
      code: 2000,
      message: 'Tên quest không được trống'
    },
    viNameRequired: {
      code: 2001,
      message: 'Tên tiếng Việt không được trống'
    },
    enNameRequired: {
      code: 2002,
      message: 'Tên tiếng Anh không được trống'
    },
    minCoin: {
      code: 2003,
      message: `Zcoin thưởng không được nhỏ hơn ${ config.strict.quest.minCoin }`
    }
  },

  // Checkin
  checkin: {
    userHasBeenBanned: {
      code: 2050,
      message: 'Tài khoản của bạn đã bị khoá chức năng tích điểm và dùng ưu đãi, vui lòng liên hệ với "Zody - Hỗ trợ" để biết thêm chi tiết!'
    },
    invalidSession: {
      code: 2051,
      message: 'Bạn đang không ở quán có thể do tín hiệu yếu. Vui lòng thử tích điểm lại!'
    },
    deviceAlreadyCheckin: {
      code: 2052,
      message: 'Thiết bị này đã tích điểm ở quán này hôm nay, vui lòng trở lại vào ngày mai!'
    },
    userAlreadyCheckin: {
      code: 2053,
      message: 'Bạn đã tích điểm ở quán này hôm nay, vui lòng trở lại vào ngày mai!'
    },
    businessHasNoEvents: {
      code: 2054,
      message: 'Địa điểm này không có chương trình tích điểm, vui lòng liên hệ với "Zody - Hỗ trợ" để biết thêm chi tiết!'
    }
  },

  // Bill
  bill: {
    invalidData: {
      code: 2100,
      message: 'UserID hoặc Số điện thoại không được trống'
    },
    billAlreadyCreated: {
      code: 2101,
      message: 'Mã hoá đơn này đã được tạo trước đây'
    },
    userNotFound: {
      code: 2102,
      message: 'Thông tin khách hàng không tìm thấy, vui lòng kiểm tra lại'
    },
    noShoppingEvent: {
      code: 2103,
      message: 'Cửa hàng này hiện không có chương trình tích điểm theo hoá đơn, vui lòng liên hệ với Zody để biết thêm chi tiết'
    },
    minPrice: {
      code: 2104,
      message: `Giá trị đơn hàng không được nhỏ hơn ${config.strict.bill.minPrice} VND`
    },
    overtimeToDelete: {
      code: 2105,
      message: `Đơn hàng chỉ có thể xoá trong vòng ${config.bill.removeExpireTime/60/60/1000}h sau khi tạo`
    },
    invalidChecksum: {
      code: 2106,
      message: 'Thông tin đơn hàng không hợp lệ'
    }
  },

  // Merchant
  merchant: {
    minNormalFrom: {
      code: 2150,
      message: `Chi tiêu tối thiểu của thành viên thường không được nhỏ hơn ${config.strict.merchant.minNormalFrom}`
    },
    minNormalTo: {
      code: 2151,
      message: `Chi tiêu tối đa của thành viên thường không được nhỏ hơn ${config.strict.merchant.minNormalTo}`
    },
    minVipFrom: {
      code: 2152,
      message: `Chi tiêu tối thiểu của thành viên VIP không được nhỏ hơn ${config.strict.merchant.minVipFrom}`
    },
    minVipTo: {
      code: 2153,
      message: `Chi tiêu tối đa của thành viên VIP không được nhỏ hơn ${config.strict.merchant.minVipTo}`
    },
    minLostDays: {
      code: 2154,
      message: `Số ngày không có giao dịch của thành viên không được nhỏ hơn ${config.strict.merchant.minLostDays}`
    }
  },

  // Chat
  chat: {
    invalidData: {
      code: 2200,
      message: 'Dữ liệu không đúng định dạng'
    },
    contentCannotEmpty: {
      code: 2201,
      message: 'Nội dung tin nhắn không được trống'
    }
  },

  // Collection
  collection: {
    nameRequired: {
      code: 2250,
      message: 'Tên collection không được trống',
      locales: {
        [config.locales.vi]: 'Tên collection không được trống',
        [config.locales.en]: ''
      }
    }
  },

  // Banner
  banner: {
    invalidType: {
      code: 2300,
      message: 'Loại banner không hợp lệ'
    }
  },

  luckyDraw: {
    goodLuck: {
      vi: 'Chúc bạn may mắn',
      en: 'Good luck to you'
    }
  },

  city: {
    invalid: `city phải thuộc ${config.city.all.split(' ')}`
  },

  // Promotion code
  promotionCode: {
    alreadyUsed: {
      code: 2350,
      message: 'Mã giảm giá này đã được sử dụng trước đây'
    },
    doesNotBelongsToThisBusiness: {
      code: 2351,
      message: 'Mã giảm giá không thuộc về địa điểm này'
    }
  },

  // Chat global
  chatGlobal: {
    invalidSendFor: {
      code: 2400,
      message: 'Đối tượng gửi không hợp lệ'
    },
    invalidSendType: {
      code: 2401,
      message: 'Loại tin nhắn gửi không hợp lệ'
    },
    invalidCity: {
      code: 2402,
      message: 'Thành phố gửi không hợp lệ'
    },
    invalidGender: {
      code: 2403,
      message: 'Giới tính gửi không hợp lệ'
    },
    contentRequired: {
      code: 2404,
      message: 'Nội dung tin nhắn không được trống'
    },
    noUsersToSend: {
      code: 2405,
      message: 'Không tìm thấy người dùng nào để gửi, vui lòng thử lại'
    },
    invalidType: {
      code: 2406,
      message: 'Cách thức gửi tin nhắn (app/sms) không hợp lệ'
    }
  },

  // Chat guide
  chatGuide: {
    invalidType: {
      code: 2450,
      message: 'Loại chat guide không hợp lệ'
    },
    contentRequired: {
      code: 2451,
      message: 'Nội dung không được trống'
    },
    answerRequired: {
      code: 2452,
      message: 'Câu trả lời không được trống'
    },
    targetRequired: {
      code: 2453,
      message: 'Target không tìm thấy'
    },
    invalidTarget: {
      code: 2454,
      message: 'Loại target không hợp lệ'
    }
  },

  // Payment transaction
  payment: {
    bankNotSupported: {
      code: 2500,
      message: 'Ngân hàng này hiện tại không được hỗ trợ để thanh toán tại Zody'
    },
    productNotFound: {
      code: 2501,
      message: 'Sản phẩm không tìm thấy hoặc số lượng đã hết, vui lòng thử lại'
    },
    topupRequirePhoneNumber: {
      code: 2502,
      message: 'Số điện thoại không đúng định dạng để thực hiện nạp tiền'
    },
    postpaidAmountIsNotValid: {
      code: 2503,
      message: 'Số tiền nạp trả sau không hợp lệ'
    },
    cannotConnectToPaymentService: {
      code: 2504,
      message: 'Không thể kết nối tới dịch vụ thanh toán online, vui lòng thử lại'
    },
    productNotEnoughQuantity: {
      code: 2505,
      message: 'Số lượng sản phẩm không đủ, vui lòng thử lại'
    }
  },

  // Plastic card
  plasticCard: {
    phoneLinked: {
      code: 2550,
      message: 'Số điện thoại này đã được liên kết với một thẻ thành viên khác. Vui lòng liên hệ với Zody để giải quyết!'
    },
    cardLinked: {
      code: 2551,
      message: 'Thẻ thành viên này đã được liên kết với một số điện thoại khác. Vui lòng liên hệ với Zody để giải quyết!'
    },
    cannotUnlink: {
      code: 2552,
      message: 'Thẻ thành viên này đã quá thời hạn để huỷ liên kết. Vui lòng liên hệ với Zody để giải quyết!'
    }
  }
}
