/**
 * Template for all strings of app
 */

export default {
  vi: {
    // Welcome message after user registered
    welcomeMessage: 'Tính năng chat giúp bạn gửi phản hồi đến chủ quán dễ dàng hơn. Có thắc mắc gì cứ hỏi Zody sẽ trả lời ngay! Cám ơn bạn đã ủng hộ và chúc bạn nhận được thật nhiều phần thưởng trên Zody!',

    // Rating message
    ratingMessage: 'Thật tuyệt vời khi được phục vụ quý khách, hãy để lại cảm nhận giúp chúng tôi phục vụ bạn 1 cách tốt hơn!',

    // Thank for rating
    thankForRating: 'Cảm ơn bạn đã gửi góp ý cho chúng tôi!',

    // Notify user about someone who invited by him checkin successfully
    referralSuccess: '%s vừa tích điểm lần đầu thành công, chúc mừng bạn nhận được %d Zcoin. Mời bạn tham gia Zody là cách nhanh nhất để nhận Zcoin đổi thưởng!',

    // Verify phone sms
    verifyPhone: 'ZodyApp - Ma xac nhan cua ban la: %s. Ma xac nhan se het han sau 15 phut.',

    // Referral social share text
    referralShareText: 'Tham gia Zody tích điểm, đổi thưởng khi đi ăn uống. Tải app tại: https://zodyapp.com/#!/dl, nhập mã giới thiệu: %s khi đăng ký để nhận điểm thưởng.',

    // Create bill success
    createBillSuccess: 'Bạn vừa nhận được %d Zcoin cho hoá đơn %s VNĐ tại cửa hàng %s. Có bất kỳ thắc mắc nào hãy hỏi "Zody - Hỗ trợ" nhé!',

    // Send SMS for user who not using zodyapp
    // billSMS: 'Ban vua nhan duoc %dZcoin = %s cho hoa don %s tai %s. Cai dat Zody: https://zodyapp.com/#!/dl de doi thuong',
    billSMS: 'Quy khach nhan duoc %d diem cho hoa don %s tai %s. Cai dat Zody https://zodyapp.com/#!/dl de doi thuong',

    // Delete bill
    deleteBill: 'Thu ngân vừa xoá giao dịch trị giá %s tại cửa hàng %s vào ngày %s, bạn bị giảm %d Zcoin cho hoá đơn trước, vui lòng liên hệ cửa hàng hoặc Zody hỗ trợ để biết thêm thông tin.',

    // Reward qr code out of stock
    rewardQrCodeOutOfStock: 'Ưu đãi QR code tại cửa hàng %s (%s) sắp hết, hãy nạp thêm.',

    // Voucher qr code out of stock
    voucherQrCodeOutOfStock: 'Voucher QR code của %s (%s) sắp hết, hãy nạp thêm.',

    // Voucher code out of stock
    voucherCodeOutOfStock: 'Voucher code của %s (%s) sắp hết, hãy nạp thêm.',

    // Voucher SMS code out of stock
    voucherSMSCodeOutOfStock: 'Voucher SMS code của %s (%s) sắp hết, hãy nạp thêm.',

    // Promotion code for app
    sendPromotionCodeApp: 'Mã giảm giá của bạn là "%s", vui lòng đưa thu ngân để sử dụng.',

    // Promotion code for SMS
    sendPromotionCodeSMS: 'Ma giam gia "%s"',

    // The coffee house intro
    theCoffeeHouseIntro: 'Tải app TCH để nhận thêm quà tặng',

    // Send confirmation code to admin
    sendConfirmationCodeToAdmin: `
      Loại tin nhắn: %s,<br/>
      Nội dung: %s,<br/>
      Mã xác nhận là: "%s", sẽ hết hạn sau 5 phút.
    `,

    // Send voucher by SMS
    voucherSMSSent: 'Thông tin quà tặng đã được gửi đến số điện thoại %s',

    // Checkin template strings
    checkin: {
      successful: 'Tích điểm thành công',
      holiday: 'Tích điểm vào ngày lễ',
      questBonus: 'Điểm thưởng nhiệm vụ',
      shareFb: 'Chia sẻ lên Facebook',
      luckyCheckin: 'Chúc mừng bạn nhận được %s cho lượt check-in thứ %d trong %s. Click sử dụng và bấm xác nhận để nhận điểm thưởng.'
    },

    // User send a photo in chat
    sentAPhoto: '%s đã gửi 1 hình ảnh'
  },
  en: {
    welcomeMessage: '',
    ratingMessage: '',
    thankForRating: '',
    referralSuccess: '',
    verifyPhone: '',
    referralShareText: '',
    createBillSuccess: '',
    billSMS: '',
    deleteBill: '',
    rewardQrCodeOutOfStock: '',
    voucherQrCodeOutOfStock: '',
    promotionCode: '',
    checkin: {
      successful: 'Check-in successful',
      holiday: 'Check-in at holiday',
      questBonus: 'Quest bonus',
      shareFb: 'Share on Facebook',
      luckyCheckin: ''
    },
    sentAPhoto: '%s sent a photo'
  }
};
