class ValidationError extends Error {
  constructor(data) {
    super(data.message)
    this.code = data.code
  }
}

export default ValidationError