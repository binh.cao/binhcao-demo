import {sign} from 'jsonwebtoken'
import config from '../config'

function generate(data, options = {expiresIn: '2d'}) {
  return sign(data.toJSON(), config.secret, options)
}

export default {
  generate
}
