import config from '../config'
import ValidationError from './error/validationError'
import commonCode from '../locales/common'

function isAuthenticated(user) {
  return (user && user._id)
}

function isBusiness(user) {
  return (user && user.roles.indexOf(config.user.roles.business) >= 0)
}

function isStaff(user) {
  return (user && user.roles.indexOf(config.user.roles.staff) >= 0)
}

function requiresStaffOrBusiness(user) {
  if (!isAuthenticated(user) || (!isStaff(user) && !isBusiness(user))) {
    throw new ValidationError(commonCode.noPermission)
  }
}

export default {
  requiresStaffOrBusiness
}