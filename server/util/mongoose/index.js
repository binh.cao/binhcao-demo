import mongoose from 'mongoose'
mongoose.Promise = global.Promise
const ObjectId = mongoose.Types.ObjectId
const Schema = mongoose.Schema

export { mongoose, ObjectId, Schema }
