import config from '../config/index'
import {verify} from 'jsonwebtoken'
const HEADER_REGEX = /bearer (.*)$/i
const whiteList = /(signIn)/i

async function authenticate(req) {
  if (new RegExp(whiteList).test(req.body.query)) {
    return ''
  }
  const {headers: {authorization}} = req
  const token = authorization && HEADER_REGEX.exec(authorization)[1]
  let decoded = await verify(token, config.secret)
  if (typeof decoded === 'string') {
    decoded = JSON.parse(decodeURIComponent(decoded))
  }
  // console.log('decoded', decoded)
  return decoded
}

export default {
  authenticate
}
