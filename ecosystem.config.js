/* jshint camelcase: false */
let appName = 'Zody_Template'
let ipServer1 = '103.199.8.77'
let ipServer2 = '103.199.8.94'
let ipServer3 = '103.199.8.95'
let ipServerDev = '139.59.246.187'
let user = 'zody'
let sshPort = '2323'
let productionBranch = 'origin/master'
let developmentBranch = 'origin/develop'
let repo = 'git@gitlab.com:binh.cao/zody-template.git'
let path = '/home/zody/zody_template'
let postDeployProduction = 'ln -nfs ../shared/.env .env && \
                      npm install --production && \
                      pm2 startOrRestart ecosystem.config.js --env production && \
                      source /home/zody/kill5007.sh'
let postDeployDev = 'ln -nfs ../shared/.env .env && \
                      npm install && \
                      pm2 startOrRestart ecosystem.config.js --env dev'

module.exports = {
  apps: [{
    name: appName,
    script: './.start.sh',
    env_production: {
      NODE_ENV: 'production'
    },
    env_dev: {
      NODE_ENV: 'development'
    }
  }],

  deploy: {
    production: {
      user: user,
      host: [{
        host: ipServer1,
        port: sshPort
      }, {
        host: ipServer2,
        port: sshPort
      }, {
        host: ipServer3,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    app1: {
      user: user,
      host: [{
        host: ipServer1,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    app2and3: {
      user: user,
      host: [{
        host: ipServer2,
        port: sshPort
      }, {
        host: ipServer3,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    dev: {
      user: user,
      host: {
        host: ipServerDev,
        port: sshPort
      },
      ref: developmentBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployDev
    }
  }
}